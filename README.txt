***
Sukurti trys useriai:
Login: admin@jura.com, Password: 123456
Login: user@jura.com, Password: 123456
Login: suspended@jura.com, Password: 123456 (suspenduotas useris)

***
Kol kas sukurtos dvi rol�s: ROLE_USER, ROLE_ADMIN (adminas turi abi roles)

***
Norint ka�kur� puslap� leisti matyti tik daliai vartotoj�,
� security-context.xml fail� reikia �ra�yti to puslapio vard�
ir roles, kurios leid�ia puslap� matyti, pvz:
        <sec:intercept-url pattern="/main.xhtml" access="hasRole('ROLE_USER')"/>
        <sec:intercept-url pattern="/admin.xhtml" access="hasRole('ROLE_ADMIN')"/>
        <sec:intercept-url pattern="/error.xhtml" access="permitAll"/>

***
Norint vaizduoti skirting� turin� viename puslapyje vartotojams
(priklausomai nuo j� rol�s: adminas, useris, suspenduotas ir tt.)
reikia prid�ti puslapiui papildom� namespace: xmlns:sec="http://www.springframework.org/security/tags"
Puslapyje naudoti tagus:
	<sec:authorize access="hasRole('ROLE_ADMIN')">
			This content will only be visible to users who have
			the "ROLE_ADMIN" authority in their list of GrantedAuthority.
	</sec:authorize>
	<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_USER')">
			This content will be visible to users who have
			the "ROLE_ADMIN" and/or 'ROLE_USER'.
	</sec:authorize>
	
***
Norint apriboti klasi� metodus, galima u�d�ti Srping-security anotacijas, pvz:
@PreAuthorize("hasRole('ROLE_ADMIN')")
@PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
P.S. kol kas ne�inau, ar verta jas d�ti, nes paspaudus ant mygtuko su tuo metodu,
puslapyje vartotojui rodo AccessDenienException stack trace'�,
kol kas nesugeb�jau to suvaldyti, reikia �gyvendinti ka�kok�
AccessDeniedHandler�, tai �ia bus darbas atei�iai.


