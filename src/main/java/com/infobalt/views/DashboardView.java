package com.infobalt.views;

import java.util.Date;
import java.util.List;

import javax.faces.application.Application;
import javax.faces.component.UIComponent;
import javax.faces.component.html.HtmlOutputText;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.primefaces.behavior.ajax.AjaxBehavior;
import org.primefaces.behavior.ajax.AjaxBehaviorListenerImpl;
import org.primefaces.component.dashboard.Dashboard;
import org.primefaces.component.link.Link;
import org.primefaces.component.panel.Panel;
import org.primefaces.event.CloseEvent;
import org.primefaces.event.DashboardReorderEvent;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;

import com.infobalt.cached.CachedTask;
import com.infobalt.entities.Task;
import com.infobalt.enums.TaskStatus;
import com.infobalt.models.SprintModel;
import com.infobalt.models.UserModel;
import com.infobalt.services.TaskService;
/**
 * Class for creating and managing drag and drop dashboard for tasks
 * @author Zygimantas
 *
 */
public class DashboardView {
	
	private Dashboard dashboard;
	private FacesContext fc;
	private Application application;
	
	private TaskService taskService;
	private SprintModel sprintModel;
	private UserModel userModel;
	private CachedTask cachedTask;
	
	
	public void init(){
		if(sprintModel.getCurrentSprint() != null){
			buildDashboard();
		}
	}
	
	/**
	 * Builds dashboard and adds listener for reorder event
	 */
	public void buildDashboard(){
		fc = FacesContext.getCurrentInstance();
		application = fc.getApplication();
		dashboard = (Dashboard) application.createComponent(fc, "org.primefaces.component.Dashboard", "org.primefaces.component.DashboardRenderer");
		dashboard.setId("dashboard");
		List<Task> tasks = taskService.findByProject(sprintModel.getCurrentSprint().getProject());
		addPanels(tasks);
		
        // add reorder listener
		AjaxBehavior ajaxBehavior = new AjaxBehavior();
		ajaxBehavior.addAjaxBehaviorListener(new ReorderPanelListener(this));
		ajaxBehavior.setTransient(true);
        ajaxBehavior.setUpdate(":sprintForm");
		dashboard.addClientBehavior("reorder", ajaxBehavior);
	}
	
	/**
	 * Builds dashboard model, creates columns for it
	 * @return model
	 */
    public DashboardModel buildModel() {

    	List<Task> tasksToDo = cachedTask.getTasksToDo(sprintModel.getCurrentSprint());
    	List<Task> tasksInProgress = cachedTask.getTasksInProgress(sprintModel.getCurrentSprint());
    	List<Task> tasksDone = cachedTask.getTasksDone(sprintModel.getCurrentSprint());
        DashboardModel model = new DefaultDashboardModel();
        DefaultDashboardColumn column1 = new DefaultDashboardColumn();
        DefaultDashboardColumn column2 = new DefaultDashboardColumn();
        DefaultDashboardColumn column3 = new DefaultDashboardColumn();
        column1.setStyleClass("columns");
        column2.setStyleClass("columns");
        column3.setStyleClass("columns");
        
        addToColumns(column1, tasksToDo);
        addToColumns(column2, tasksInProgress);
        addToColumns(column3, tasksDone);
       
        model.addColumn(column1);
        model.addColumn(column2);
        model.addColumn(column3);

		return model;	
    }
    
    /**
     * Iterates through list of tasks, creates panels, adds close listener to them;
     * @param tasks list of tasks
     */
    public void addPanels(List<Task> tasks){
        for (Task task: tasks){
			
			Panel panel = createPanel(task);
			
			dashboard.getChildren().add(panel);
		
        }
    }
    
    /**
     * Adds dashboard panels from dashboard to column, if there are provided tasks for these panels
     * @param column Column to which panels will be added
     * @param tasks List of tasks for the column
     */
    public void addToColumns(DefaultDashboardColumn column, List<Task> tasks){
		
    	List<UIComponent> panels = dashboard.getChildren();
		
    	for (Task task: tasks){
			for (UIComponent panel: panels){
				panel = (Panel) panel;
				if (panel.getId().equals("panel_" + task.getId())){
					column.addWidget(panel.getId());

				}
			}
		}

    }
    
    /**
     * Creates panel, sets its id, widgetVar and info about the visualized task 
     * @param task Task to be visualized
     * @return Panel for the dashboard
     */
    public Panel createPanel(Task task){
    	Panel panel = (Panel) application.createComponent(fc, "org.primefaces.component.Panel", "org.primefaces.component.PanelRenderer");
		panel.setId("panel_" + task.getId());
		panel.setWidgetVar("panel_" + task.getId());
		panel.setHeader("TASK-" + task.getId().toString());
		panel.setClosable(true);
		panel.setCloseTitle("Remove from sprint");
		HtmlOutputText text = new HtmlOutputText();
		if (task.getAssignee() != null){
			text.setValue(task.getAssignee().getName() + " "  + task.getAssignee().getSurname());
		}
		
		HtmlOutputText esc = new HtmlOutputText();
		esc.setValue("<br/>");
		esc.setEscape(false);
		
		Link link = new Link();
		link.setOutcome("task?faces-redirect=true&amp;id=" + task.getId());
		link.setValue(task.getTitle());
		link.setStyleClass("ui-panel-task-name");
		panel.getChildren().add(link);
		panel.getChildren().add(esc);
		panel.getChildren().add(text);
		
		//add close listener
		AjaxBehavior ajaxBehavior = new AjaxBehavior();
		ajaxBehavior.addAjaxBehaviorListener(new ClosePanelListener(this));
		ajaxBehavior.setTransient(true);
		ajaxBehavior.setUpdate(":sprintForm");
		panel.addClientBehavior("close", ajaxBehavior);
		
		return panel;
    }
    
    /**
     * Handles reorder, when reorder event is fired:
     * Changes task status according to which column panel is dragged
     * @param event
     */
    public void handleReorder(DashboardReorderEvent event){
    	String panelId = event.getWidgetId();
    	int columnId = event.getColumnIndex();
    	int id = Integer.parseInt(panelId.split("_")[1]);
    	Task task = taskService.findById(id);
    	
    	if (columnId == 0){
    		task.setTaskStatus(TaskStatus.TODO);
    	} else if (columnId == 1){
    		task.setTaskStatus(TaskStatus.INPROGRESS);
    	} else if (columnId == 2){
    		task.setTaskStatus(TaskStatus.DONE);
    	}
    	taskService.changeStatus(task, userModel.getCurrentUser());
    	cachedTask.reset();
    	
    }
    
    /**
     * Handles close
     * Removes task from the sprint
     * @param event
     */
    public void handleClose(CloseEvent event){
    	Panel panel = (Panel) event.getSource();
    	String panelId = panel.getId();
    	int id = Integer.parseInt(panelId.split("_")[1]);
    	Task task = taskService.findById(id);
    	task.setSprint(null);
		task.setUpdateDate(new Date());
		taskService.save(task);
		cachedTask.reset();
		
		panel.setRendered(true);
		panel.setVisible(true);

    }
    
    /**
     * Listener for closing task panels
     * @author Zygimantas
     *
     */
    public static class ClosePanelListener extends AjaxBehaviorListenerImpl{
		private static final long serialVersionUID = -7840908538712108723L;
		private DashboardView view;
    	
    	public ClosePanelListener(DashboardView view) {
			this.view = view;
		}
		@Override
    	public void processAjaxBehavior (AjaxBehaviorEvent event){
    		view.handleClose((CloseEvent)event);
    	}
    }
    
    /**
     * Listener class for reordering panels
     * @author Zygimantas
     *
     */
    public static class ReorderPanelListener extends AjaxBehaviorListenerImpl{
		private static final long serialVersionUID = 4484098265933951823L;
		private DashboardView view;
		
		public ReorderPanelListener(DashboardView view) {
			this.view = view;
		}
		@Override
		public void processAjaxBehavior (AjaxBehaviorEvent event){
			view.handleReorder((DashboardReorderEvent)event);
		}
    	
    }

	public DashboardModel getModel() {
		return buildModel();
	}

	public void setModel(DashboardModel model) {

	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}

	public SprintModel getSprintModel() {
		return sprintModel;
	}

	public void setSprintModel(SprintModel sprintModel) {
		this.sprintModel = sprintModel;
	}

	public Dashboard getDashboard() {
		return dashboard;
	}

	public void setDashboard(Dashboard dashboard) {
		this.dashboard = dashboard;
	}

	public CachedTask getCachedTask() {
		return cachedTask;
	}

	public void setCachedTask(CachedTask cachedTask) {
		this.cachedTask = cachedTask;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}
     
}
