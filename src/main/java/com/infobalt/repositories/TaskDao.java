package com.infobalt.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.entities.Task;
import com.infobalt.entities.User;
import com.infobalt.enums.TaskStatus;

public class TaskDao {
	private EntityManagerFactory entityManagerFactory;
	private EntityManager em;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
		this.em = entityManagerFactory.createEntityManager();
	}

	public void save(Task newTask) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		if (!em.contains(newTask))
			newTask = em.merge(newTask);
		em.persist(newTask);
		em.getTransaction().commit();
		em.close();
	}

	public void delete(Task task) {
		em = entityManagerFactory.createEntityManager();
		em.getTransaction().begin();
		task = em.merge(task);
		em.remove(task);
		em.getTransaction().commit();
		em.close();
	}

	public List<Task> findAll() {
		em = entityManagerFactory.createEntityManager();
		try {
			CriteriaBuilder cb = em.getCriteriaBuilder();
			CriteriaQuery<Task> cq = cb.createQuery(Task.class);
			Root<Task> root = cq.from(Task.class);
			cq.select(root);
			TypedQuery<Task> q = em.createQuery(cq);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findUserTasks(User user){
		em = entityManagerFactory.createEntityManager();
		TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.assignee = :assignee ORDER BY t.updateDate DESC", Task.class);
		query.setParameter("assignee", user);
		return query.getResultList();
	}

	public Task findById(Integer id) {
		em = entityManagerFactory.createEntityManager();
		try {
			return em.find(Task.class, id);
		} finally {
			em.close();
		}

	}
	
	public List<Task> findByProject(Project project) {
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.project = :project ORDER BY t.updateDate DESC", Task.class);
			query.setParameter("project", project);
			return query.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findByProjectAndStatus(Project project, TaskStatus taskStatus) {
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.project = :project AND t.taskStatus = :taskStatus", Task.class);
			query.setParameter("project", project);
			query.setParameter("taskStatus", taskStatus);
			return query.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findBySprint(Sprint sprint) {
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.sprint = :sprint  ORDER BY t.updateDate DESC", Task.class);
			query.setParameter("sprint", sprint);
			return query.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findSuspendedUsersTasksInProject(Project project){
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.assignee.active = :active AND t.project.id = :projectId AND t.taskStatus != :status", Task.class);
			query.setParameter("active", false);
			query.setParameter("projectId", project.getId());
			query.setParameter("status", TaskStatus.DONE);
			return query.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findBySprintAndStatus(Sprint sprint, TaskStatus taskStatus){
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.sprint = :sprint AND t.taskStatus = :taskStatus ORDER BY t.updateDate DESC", Task.class);
			query.setParameter("sprint", sprint);
			query.setParameter("taskStatus", taskStatus);
			return query.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findByProjectAndUserWithStatusNotDone(Project project, User user){
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.project.id = :projectId AND t.taskStatus != :taskStatus AND t.assignee.id = :assigneeId", Task.class);
			query.setParameter("projectId", project.getId());
			query.setParameter("taskStatus", TaskStatus.DONE);
			query.setParameter("assigneeId", user.getId());
			return query.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findNotInSprint(Project project) {
		try{
			em = entityManagerFactory.createEntityManager();
			TypedQuery<Task> query = em.createQuery("SELECT t From Task t WHERE t.project = :project AND t.sprint IS NULL ORDER BY t.creationDate DESC", Task.class);
			query.setParameter("project", project);
			return query.getResultList();
		} finally {
			em.close();
		}
	}

	public List<Task> findTasksByTitlePattern(String query) {
		em = entityManagerFactory.createEntityManager();
		try {
			TypedQuery<Task> q = em.createQuery(
					"SELECT t FROM Task t WHERE LOWER(CONCAT(t.title, t.taskStatus)) LIKE :pattern", Task.class);
			q.setParameter("pattern", "%" + query.toLowerCase() + "%");
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
	public List<Task> findByDescriptionFragment(String query, Project project) {
		em = entityManagerFactory.createEntityManager();
		try {
			TypedQuery<Task> q = em.createQuery(
					"SELECT t FROM Task t WHERE t.project = :project AND LOWER(t.description) LIKE :pattern", Task.class);
			q.setParameter("pattern", "%" + query.toLowerCase() + "%");
			q.setParameter("project", project);
			return q.getResultList();
		} finally {
			em.close();
		}
	}
	
}
