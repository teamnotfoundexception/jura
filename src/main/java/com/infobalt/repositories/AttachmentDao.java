package com.infobalt.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.TypedQuery;

import com.infobalt.entities.Attachment;
import com.infobalt.entities.Task;
import com.infobalt.transfer_objects.AttachmentDetails;

public class AttachmentDao {

	private EntityManagerFactory entityManagerFactory;

	public EntityManagerFactory getEntityManagerFactory() {
		return entityManagerFactory;
	}

	public void setEntityManagerFactory(EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	public void save(Attachment attachment) {
		EntityManager em = entityManagerFactory.createEntityManager();
		try {
			em.getTransaction().begin();
			if (!em.contains(attachment))
				attachment = em.merge(attachment);
			em.persist(attachment);
			em.getTransaction().commit();
		} finally {
			em.close();
		}
	}

	public List<AttachmentDetails> findAttchmetDetailsListByTask(Task task) {
		EntityManager em = entityManagerFactory.createEntityManager();
		TypedQuery<AttachmentDetails> query = em.createQuery(
				"SELECT NEW com.infobalt.transfer_objects.AttachmentDetails(a.id, a.name, LENGTH(a.data), a.mimeType) FROM Attachment a WHERE a.task = :task ORDER BY a.name",
				AttachmentDetails.class);
		query.setParameter("task", task);
		return query.getResultList();
	}
	
	public Attachment findById(Integer id) {
		EntityManager em = entityManagerFactory.createEntityManager();
		TypedQuery<Attachment> query = em.createQuery("SELECT a FROM Attachment a WHERE a.id = :id", Attachment.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

}
