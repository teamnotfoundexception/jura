package com.infobalt.validators;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.infobalt.entities.User;
import com.infobalt.services.UserService;

/**
 * Custom validator for unique user login name.
 * @author Zygimantas
 *
 */
public class UniqueLoginValidator implements Validator{
	
	UserService userService;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
		
		if (value == null) {
            return; // Let required="true" handle.
        }
		
		String login = (String) value;
		
		List<User> users = userService.findAll();
		
		for(User user: users){
			if (login.equals(user.getLogin())){
				throw new ValidatorException( new FacesMessage(
						FacesMessage.SEVERITY_ERROR, "There already is a user with such login name.", "There already is a user with such login name."));
			}
		}
		
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
