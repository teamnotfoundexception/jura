package com.infobalt.validators;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.infobalt.entities.Sprint;
import com.infobalt.models.SprintModel;
import com.infobalt.services.SprintService;

/**
 * Custom validator for date ranges (
 * second date must be after first date;
 * dates of different sprints must not intersect)
 * @author Zygimantas
 *
 */
public class SprintDatesValidator implements Validator{
	
	private SprintService sprintService;
	private SprintModel sprintModel;

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {
        
		if (value == null) {
            return; // Let required="true" handle.
        }
		
        Date startDate;
        Date endDate;
        Sprint currentSprint = sprintModel.getCurrentSprint();
        
        
        //Check which component invoked validator and assign start and end values
        if (component.getId().equals("start")){
        	startDate = (Date) value;
        	endDate = currentSprint.getFinishDate();
        	
        } else if (component.getId().equals("finish")){
        	startDate = currentSprint.getStartDate();
        	endDate = (Date) value;
        	
        } else if (component.getId().equals("add_finish")){
        	UIInput startDateComponent = (UIInput) component.getAttributes().get("startDateComponent");
        	
        	if (!startDateComponent.isValid()) {
                return; // Already invalidated. Don't care about it then.
            }
        	
        	startDate = (Date) startDateComponent.getValue();
        	
            if (startDate == null) {
                return; // Let required="true" handle.
            }
            
            endDate = (Date) value;
            
        } else {
        	return;
        }

        
        List<Sprint> sprints = sprintService.findByProject(currentSprint.getProject());
       
        
        // check if end date is not before start date
        if (endDate.before(startDate)){
        	throw new ValidatorException(new FacesMessage(
                    FacesMessage.SEVERITY_ERROR, "Start date may not be after end date.", "Please, check entered date"));
        }
        
        //iterate through all sprints in the project
        for (Sprint sprint: sprints){
        	if (sprint.getId().equals(currentSprint.getId())){
        		continue;
        	}
        	Date otherStart = sprint.getStartDate();
        	Date otherFinish = sprint.getFinishDate();
        	
        	// check if start date is not inside other sprint date range
        	if (startDate.after(otherStart) && startDate.before(otherFinish)){
        		throw new ValidatorException( new FacesMessage(FacesMessage.SEVERITY_ERROR,
        				"Start date is in the range of other sprint", "Please, check entered date"));
        	}
        	
        	//check if end date is not in the range of other sprint
        	if (endDate.after(otherStart) && endDate.before(otherFinish)){
        		throw new ValidatorException( new FacesMessage(FacesMessage.SEVERITY_ERROR,
        				"End date is in the range of other sprint", "Please, check entered date"));
        	}
        	
        	//check if there aren't any sprints inside this date range
        	if (otherStart.after(startDate) && otherStart.before(endDate)){
        		throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
        				"There is other sprint inside this date range", "Please, check entered date"));
        	}
        }
        
        // check if sprint dates do not conflict with project dates
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
        Date projectStartDate = sprintModel.getCurrentSprint().getProject().getStartDate();
        if (startDate.compareTo(projectStartDate) < 0) {
        	String date = sdf.format(projectStartDate);
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Start date error:", "Sprint cannot be started earlier than the project (" + date +  ")."));
        }
        Date projectEndDate = sprintModel.getCurrentSprint().getProject().getFinishDate();
        if (endDate.compareTo(projectEndDate) > 0) {
        	String date = sdf.format(projectEndDate);
        	throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "End date error:", "Sprint cannot be ended  later than the project (" + date +  ")."));
        }
		
	}
	
	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

	public SprintModel getSprintModel() {
		return sprintModel;
	}

	public void setSprintModel(SprintModel sprintModel) {
		this.sprintModel = sprintModel;
	}

	
}
