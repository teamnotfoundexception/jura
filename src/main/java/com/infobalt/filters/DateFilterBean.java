package com.infobalt.filters;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


/**
 * Custom filter bean for Primefaces DataTable filtering
 * @author Zygimantas
 *
 */
public class DateFilterBean {

	/**
	 * Compares value date with filter range dates;
	 * returns true if value falls into the range, 
	 * false if value is out of range
	 * @param value - value to be checked
	 * @param filter - date range
	 * @param locale - date locale
	 * @return true if value in the range, false if out of range
	 */
	public boolean filterByDate(Object value, Object filter, Locale locale) {
		
		String filterText = (filter == null) ? null : filter.toString().trim();		
		
		if (filterText == null || filterText.isEmpty()) {
			return true;
		}
		
		if (value == null && filterText.equals(":")){
			return true;
		}
		
		if (value == null) {
			return false;
		}

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		Date filterDate = (Date) value;
		Date dateFrom;
		Date dateTo;
		try {
			String fromPart = filterText.substring(0, filterText.indexOf(":"));
			String toPart = filterText.substring(filterText.indexOf(":") + 1);
			dateFrom = fromPart.isEmpty() ? null : df.parse(fromPart);
			dateTo = toPart.isEmpty() ? null : df.parse(toPart);
		} catch (ParseException pe) {
			return false;
		}

		return (dateFrom == null || filterDate.after(dateFrom)) && (dateTo == null || filterDate.before(dateTo));
	}
}
