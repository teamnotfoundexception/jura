package com.infobalt.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.infobalt.entities.User;
import com.infobalt.repositories.UserDao;
/**
 * The main purpose of the class is to map User class of 
 * the application to the User class of Spring Security.
 * 
 * @author Zygimantas
 */

public class CustomUserDetailsService implements UserDetailsService {
	
	private UserDao userDao;

	/**
	 * this method maps com.infobalt.entities.User class to
	 * org.springframework.security.core.userdetails.User class
	 */
	@Override
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        
		User domainUser = userDao.getUser(login);
        
        boolean accountNonLocked = false;
        if (domainUser.isActive()){
        	accountNonLocked = true;
        } 
        boolean accountNonExpired = true;
        boolean credentialsNonExpired = true;
        boolean enabled = true;

        return new UserPrincipal(
            domainUser.getLogin(),
            domainUser.getPassword(),
            enabled,
            accountNonExpired,
            credentialsNonExpired,
            accountNonLocked,
            getAuthorities(domainUser.getRole().getId()),
            domainUser
        );
 
	}
	
	public Collection<? extends GrantedAuthority> getAuthorities(Integer role) {
        List<GrantedAuthority> authList = getGrantedAuthorities(getRoles(role));
        return authList;
    }
   /*
    * In this method we hardcode Spring-security roles
    * and map them to a Role entities
    * according to their id's.
    * @param role 
    */
    public List<String> getRoles(Integer role) {

        List<String> roles = new ArrayList<String>();

        if (role.intValue() == 1) {
            roles.add("ROLE_USER");
            roles.add("ROLE_ADMIN");
        } else if (role.intValue() == 2) {
            roles.add("ROLE_USER");
        }
        return roles;
    }
   
    public static List<GrantedAuthority> getGrantedAuthorities(List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
       
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}  

}
