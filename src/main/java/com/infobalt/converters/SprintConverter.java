package com.infobalt.converters;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

import com.infobalt.entities.Sprint;
import com.infobalt.services.SprintService;

/**
 * Custom converter for Sprint entities
 * @author Zygimantas
 *
 */
public class SprintConverter implements Converter {

	SprintService sprintService;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {
		if (submittedValue == null || submittedValue.isEmpty()) {
	        return null;
	    }

	    try {
	        return sprintService.findById(Integer.valueOf(submittedValue));
	    } catch (NumberFormatException e) {
	        throw new ConverterException(new FacesMessage(submittedValue + " is not a valid Sprint ID"), e);
	    }
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
		if (modelValue == null) {
	        return "";
	    }

	    if (modelValue instanceof Sprint) {
	        return String.valueOf(((Sprint) modelValue).getId());
	    } else {
	        throw new ConverterException(new FacesMessage(modelValue + " is not a valid Sprint"));
	    }
	}

	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}
	
}
