package com.infobalt.models;

import com.infobalt.entities.Comment;

public class CommentModel {

	private Comment currentComment;

	public Comment getCurrentComment() {
		return currentComment;
	}

	public void setCurrentComment(Comment currentComment) {
		this.currentComment = currentComment;
	}

}
