package com.infobalt.cached;

import java.util.List;

import com.infobalt.entities.Project;
import com.infobalt.entities.User;
import com.infobalt.services.ProjectService;

public class CachedProject {

	ProjectService projectService;
	private List<Project> projects;
	private List<Project> managedProjects;
	private List<Project> userProjects;

	public List<Project> getProjects() {
		if (projects == null) {
			projects = projectService.findAll();
		}
		return projects;
	}
	
	public List<Project> getManagedProjects(User user) {
		if (managedProjects == null) {
			managedProjects = projectService.findUserManagedProjects(user);
		}
		return managedProjects;
	}
	
	public List<Project> getUserProjects(User user){
		if (userProjects == null) {
			userProjects = projectService.getUserProjects(user);
		}
		return userProjects;
	}

	public ProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(ProjectService projectService) {
		this.projectService = projectService;
	}
	
}
