package com.infobalt.controllers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.infobalt.cached.CachedTask;
import com.infobalt.entities.Project;
import com.infobalt.entities.Sprint;
import com.infobalt.entities.Task;

import com.infobalt.entities.User;
import com.infobalt.models.ProjectModel;
import com.infobalt.models.SprintModel;
import com.infobalt.models.TaskModel;
import com.infobalt.models.UserModel;
import com.infobalt.services.SprintService;
import com.infobalt.services.TaskService;

public class SprintController {
	
	private static final Logger logger = LogManager.getLogger(UserController.class.getName());
	
	private SprintModel sprintModel;
	private SprintService sprintService;
	private ProjectModel projectModel;
	private TaskService taskService;
	private TaskModel taskModel;
	private CachedTask cachedTask;
	private UserModel userModel;
	
	public String create(){
		Sprint sprint = new Sprint();
		sprint.setProject(projectModel.getCurrentProject());
		sprintModel.setCurrentSprint(sprint);
		return "addSprint";
	}
	
	public String update(Sprint sprint){
		sprintModel.setCurrentSprint(sprint);
		return "sprint-update";
	}
	
	public String save(){
		sprintService.save(sprintModel.getCurrentSprint());
		if(sprintModel.getCurrentSprint().getId() == null){
			logger.info("New sprint in project: " + "'" + projectModel.getCurrentProject().getId()
					+ "'" + " was created by: " + "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		else {
			logger.info("Sprint in project: " + "'" + projectModel.getCurrentProject().getId()
					+ "'" + " was updated by: " + "'" + userModel.getLoggedUser().showFullName() + "'");
		}
		return "sprint-list";
	}
	
	public String cancel(){
		sprintModel.setCurrentSprint(null);
		return "sprint-list";
	}
	
	public void delete(Sprint sprint){
		sprintService.delete(sprint);
	}
	
	public String overview(Sprint sprint){
		sprintModel.setCurrentSprint(sprint);
		return "overviewSprint";
	}
	
	public List<Sprint> findAll() {
		return sprintService.findAll();
	}
	
	public Sprint findById(Integer id){
		return sprintService.findById(id);
	}
	
	public List<Sprint> findByProject(Project project){
		return sprintService.findByProject(project);
	}
	/**
	 * This method displays sprint view page, if logged user is project worker, project manager or admin.
	 * @return returns true - if page is displayed
	 */
	public boolean displayPage(){
		User loggedUser = null;
		Project project = null;
		List<User> users = null;
				
		try {
			loggedUser = userModel.getLoggedUser();
			project = sprintModel.getCurrentSprint().getProject();
			users = project.getUsers();
			
			//display always to admin or project manager
			if(project.getProjectManager().getId() == loggedUser.getId() || loggedUser.getRole().getRole().equals("admin")){
				return true;
			}
			
			if(users != null && users.size() != 0){
				for(User u : users){
					if(u.getId().equals(loggedUser.getId())){
						return true;
					}
				}
			}	
		} catch (NullPointerException e) {
			return false;
		}
				
		return false;
	}
	
	/**
	 * Returns list of sprints in current project 
	 * by provided name pattern
	 * @param query - String name pattern
	 * @return - list of sprints
	 */
	public List<Sprint> autoComplete(String query){
		return sprintService.findSprintsByNamePattern(taskModel.getCurrentTask().getProject(), query);
	}
	
	/**
	 * Counts number of tasks in current sprint
	 * @return int Number of tasks
	 */
	public int countTasks(){
		int count = cachedTask.getTasksInSprint(sprintModel.getCurrentSprint()).size();
		return count;
	}
	
	/**
	 * Counts number of tasks with status DONE in current sprint
	 * @return int Number of tasks
	 */
	public int countDoneTasks(){
		int count = cachedTask.getTasksDone(sprintModel.getCurrentSprint()).size();
		return count;
	}
	
	/**
	 * Counts number of tasks with status INPROGRESS in current sprint
	 * @return int Number of tasks
	 */
	public int countInProgressTasks(){
		int count = cachedTask.getTasksInProgress(sprintModel.getCurrentSprint()).size();
		return count;
	}
	
	/**
	 * Counts number of tasks with status TODO in current sprint
	 * @return int Number of tasks
	 */
	public int countToDoTasks(){
		int count = cachedTask.getTasksToDo(sprintModel.getCurrentSprint()).size();
		return count;
	}
	
	/**
	 * Calculates number of story points in current sprint
	 * @return int Story points
	 */
	public int countStoryPoints(){
		int storyPoints = 0;
		List<Task> tasks = cachedTask.getTasksInSprint(sprintModel.getCurrentSprint());
		for (Task task: tasks){
			storyPoints = storyPoints + task.getStoryPoints();
		}
		return storyPoints;
	}
	
	/**
	 * Calculates number of story points of tasks with status DONE in current sprint
	 * @return int Story points
	 */
	public int countStoryPointsDone(){
		int storyPoints = 0;
		List<Task> tasks = cachedTask.getTasksDone(sprintModel.getCurrentSprint());
		for (Task task: tasks){
			storyPoints = storyPoints + task.getStoryPoints();
		}
		return storyPoints;
	}
	
	/**
	 * Calculates number of story points of tasks with status INPROGRESS AND TODO in current sprint
	 * @return int Story points
	 */
	public int countStoryPointsLeft(){
		return countStoryPoints() - countStoryPointsDone();
	}
	
	/**
	 * Returns true if sprint is finished, but has unfinished tasks.
	 * Returns false if everything is finished, or sprint is not finished yet, or null was passed through as a parameter.
	 * @param sprint Sprint to be checked
	 * @return boolean Sprint failed
	 */
	public boolean isSprintFailed(Sprint sprint){
		return sprintService.isSprintFailed(sprint);
	}	
	
	/**
	 * Generates and exports current sprint tasks to .csv file.
	 * @return streamed content of generated file.
	 * @throws FileNotFoundException if file was not found.
	 */
	public StreamedContent generateCsvFile() throws FileNotFoundException{
		String tmpdir = System.getProperty("java.io.tmpdir");
		File file = null;
		List<Task> taskList = cachedTask.getTasksInSprint(sprintModel.getCurrentSprint());
		BufferedWriter writer = null;
		
		StreamedContent download = null;
	    InputStream inputStream = null;
	    ExternalContext externalContext = null;
		//generate .csv file
		try {
			file = new File(tmpdir+"/"+ projectModel.getCurrentProject().getTitle() +".csv");
			writer = new BufferedWriter(new FileWriter(file));
			
			writer.write("ID");
			writer.write(',');
			writer.write("Title");
			writer.write(',');
			writer.write("StoryPoints");
			writer.write(',');
			writer.write("Assigned to");
			writer.write(',');
			writer.write("Status");
			writer.write(',');
			writer.write("Created");
			writer.write(',');
			writer.write("Updated");
			writer.write(',');
			writer.write("Completed");
			writer.newLine();
			
			for(Task task : taskList){
				writer.write(validateWriter(task.getId()));
				writer.write(',');
				writer.write(validateWriter(task.getTitle()));
				writer.write(',');
				writer.write(validateWriter(String.valueOf(task.getStoryPoints())));
				writer.write(',');
				try {
					writer.write(validateWriter(task.getAssignee().showFullName()));
				} catch (NullPointerException e) {
					writer.write("");
					e.getMessage();
				}
				writer.write(',');
				writer.write(validateWriter(task.getTaskStatus().getValue()));
				writer.write(',');
				writer.write(validateWriter(task.getCreationDate()));
				writer.write(',');
				writer.write(validateWriter(task.getUpdateDate()));
				writer.write(',');
				writer.write(validateWriter(task.getCompleteDate()));
				writer.newLine();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	    //export .csv file
	    try {
	    	download = new DefaultStreamedContent();
	    	inputStream = new FileInputStream(file);
	    	externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    	download = new DefaultStreamedContent(inputStream, externalContext.getMimeType(file.getName()), file.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return download;
	}
	/**
	 * Validates if object is not null, if so, returns an empty string
	 * @param value - given object
	 * @return - object converted to string
	 */
	private String validateWriter(Object value){
		try {
			if(value != null){
				return value.toString();
			}
		} catch (NullPointerException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * Handles select event on datatable row,
	 * navigates to selected item page
	 * @param event SelectEvent
	 */
	public void onRowSelect(SelectEvent event) {
		sprintModel.setCurrentSprint((Sprint) event.getObject());
		ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance()
				.getApplication().getNavigationHandler();
		handler.performNavigation("overviewSprint");
	}
	
	public SprintModel getSprintModel() {
		return sprintModel;
	}
	
	public void setSprintModel(SprintModel sprintModel) {
		this.sprintModel = sprintModel;
	}
	
	public SprintService getSprintService() {
		return sprintService;
	}
	
	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

	public ProjectModel getProjectModel() {
		return projectModel;
	}

	public void setProjectModel(ProjectModel projectModel) {
		this.projectModel = projectModel;
	}

	public TaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(TaskService taskService) {
		this.taskService = taskService;
	}
	
	public TaskModel getTaskModel() {
		return taskModel;
	}
	
	public void setTaskModel(TaskModel taskModel) {
		this.taskModel = taskModel;
	}

	public CachedTask getCachedTask() {
		return cachedTask;
	}

	public void setCachedTask(CachedTask cachedTask) {
		this.cachedTask = cachedTask;
	}

	public UserModel getUserModel() {
		return userModel;
	}

	public void setUserModel(UserModel userModel) {
		this.userModel = userModel;
	}
	

}
