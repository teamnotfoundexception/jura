package com.infobalt.controllers;

public class TabController {

	public Integer adminViewTabIndex = 0;
	public Integer userViewTabIndex = 0;
	
	public Integer getAdminViewTabIndex() {
		return adminViewTabIndex;
	}
	public void setAdminViewTabIndex(Integer adminViewTabIndex) {
		this.adminViewTabIndex = adminViewTabIndex;
	}
	public Integer getUserViewTabIndex() {
		return userViewTabIndex;
	}
	public void setUserViewTabIndex(Integer userViewTabIndex) {
		this.userViewTabIndex = userViewTabIndex;
	}

}
